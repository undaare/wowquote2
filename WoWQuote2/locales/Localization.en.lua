﻿if (not("deDE" == GetLocale())) then

WQ2_BASE_TAGS = {
	--["hi"] = "Hi",
	--["bye"] = "Bye",
	--["positive"] = "Emotions (positive)",
	--["negative"] = "Emotions (negative)",
	--["pvp"] = "PvP",
	--["pve"] = "PvE",
	--["announce"] = "Announcements",
	--["group"] = "In a party",
	--["music"] = "Music",
	--["priest"] = "Priest",
	--["hunter"] = "Hunter",
	--["rogue"] = "Rogue",
	--["warlock"] = "Warlock",
	--["mage"] = "Mage",
	--["paladin"] = "Paladin",
	--["schaman"] = "Shaman",
	--["warrior"] = "Warrior",
	--["druid"] = "Druid",
	--["women"] = "Woman",
	--["men"] = "Men",
	--["pause"] = "Pause",
	--["music"] = "Music",
	--["sound"] = "Sound",
};

WQ2_channels = {
    ["GUILD"] = { ["name"] = "Guild", ["color"] = "|cff40ff40" },
    ["PARTY"] = { ["name"] = "Group", ["color"] = "|cffaaaaff" },
    ["RAID"] = { ["name"] = "Raid", ["color"] = "|cffff7f00"},
    ["BATTLEGROUND"] = { ["name"] = "Battleground" , ["color"] = "|cffff7f00" },
};

WQ2_HELP = {
    "--- WoWQuote Help ---\n",
	"|cffffffff- /wqh : |r |cff7090ffShows this help text",
    "|cffffffff- /wqb [channel] [on|off]: |r |cff7090ffShows or configures all channels you are listening to for quotes. Possilble channels: p(arty), r(aid) oder g(uild). \"all\" to turn off/on all channels",
    "|cffffffff- /wqf <String>: |r |cff7090ffLists all quotes, that contain the entered string",
    "|cffffffff- /wqa <Name> [Quote-ID]: |r |cff7090ffDefines a user defined name for the entered quote",
    "|cffffffff- /wqp <Quote-ID or Name>: |r |cff7090ffSend Quote to the Group channel",
    "|cffffffff- /wqg <Quote-ID or Name>: |r |cff7090ffSend Quote to the Guild channel",
    "|cffffffff- /wqr <Quote-ID or Name>: |r |cff7090ffSend Quote to the Raid channel",
    "|cffffffff- /wql <Quote-ID or Name>: |r |cff7090ffTest Quote local test listening",
	"|cffffffff- /wqs: |r |cff7090ffStops the current Music file",
  "|cffffffff- /wqqb |r |cff7090ffHides the Quickbutton until enabled again",
};

WQ2_MSG = {
	["msg_loaded"] = "%s v%s loaded. type /wqh for help, /wq to show the GUI",
	["msg_cat_title"] = "%s - Available Categories:",
	["msg_conf_title"] = "%s - Available Broadcast-settings:",
	["msg_qlist_title"] = "\n%s - Quotes containing '%s':",
	["err_cat_id"] = "Invalid Category-ID! /wqc lists all valid categories.",
	["err_quote_not_found"] = "Quote-ID \"%s\" not found!",
	["err_miss_channel"] = "Channel has to be  p, r or g",
	["err_miss_switch"] = "Please enter on or off",
	["err_wrong_alias"] = "Invalid name. Only enter a combination of letters and numbers. The first letter has to be a letter. The name may only be " .. WQ2_MAX_ALIAS_LEN .. " long.", -- m�glicherweise nicht erlaubt
	["err_alias_not_found"] = "Quote-ID %s has no defined name.",
	["msg_alias_disabled"] = "User defined name '%s' removed.",
	["msg_alias_set"] = "Name '%s' was set for Quote-ID '%s'.",
	["msg_chan_on"] = "Receiver activated for '%s'.",
	["msg_chan_off"] = "Receiver deactivated for '%s'.",
	["err_search_len"] = "String has to be at least %s long.",
	["msg_search_count"] = "%s Quotes found.",
    ["err_sendchan_off"] = "Could not send because the channel |cffffffff%s|cff7090ff is deactivated.",
    ["err_oldWQ2_loaded"] = "|cffff0000An old version of WoWQuote was detected please remove it!",
    ["err_not_in_party"] = "You are not in a group!",
    ["msg_show_aliases"] = "Currently user defined names:",
	["msg_fav_set"] = "Quote-ID '%s' has been set as Favorite.",
	["msg_fav_unset"] = "Quote-ID '%s' has been unset as Favorite.",
};

end
