if ( not("deDE" == GetLocale()) ) then

	WoWQuote2UI_Localization = {
		DIALOG_TITLE = "WoWQuote2",
		DIALOG_TITLE_NEW_VERSION = "New version %s available",
		COLUMN_TEXT = "Quotes",
		COLUMN_t = "Duration",
		COLUMN_DURATION = "ID",
		COLUMN_tag = "Tag",
		COLUMN_FAV = "Fav",
		BINDING_HEADER = "WoWQuote2",
		BINDING_TOGGLE = "WoWQuote2 UI toggle",
		SEARCH = "Search...",
		BUTTON_STOPBUTTON = "Stop Music",
		CHECKBUTTON_GUILD = "Guild",
		CHECKBUTTON_PARTY = "Party",
		CHECKBUTTON_RAID = "Raid",
		TEST_LISTEN_CHANNEL = "Test-Mode",
		TOOLTIP_GUILD = "|cff40ff40Guild|r",
		TOOLTIP_PARTY = "|cffaaaaffParty|r",
		TOOLTIP_RAID = "|cffff7f00Raid|r",
		TOOLTIPCHANNEL = "If this box is unchecked you won't receive any Sounds that are being sent via ",
		TOOLTIPCHANNEL2 = "-Channel",
		TOOLTIP_STOPBUTTON = "Stops music, if any music is currently playing, you can also use the command |cff7090ff/wqs|r to stop music files",
		TOOLTIP_CHANNELDROPDOWN = "Choose the channel in which you want to send sounds"
	}
end
