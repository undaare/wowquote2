## Title: WoWQuote2
## Version: 0.3.1 rumchiller-port0.2
## Notes: Addon to play back sound quotes in game.
## Notes-deDE: Addon zum abspielen lustiger Soundzitate.
## Interface: 11304
## Author: WQ1 - Lev@Die Ewige Wacht, Harag@Die Ewige Wacht, WQ2 - Mukura@Der Rat von Dalaran, Salandril/Barakaitos@Garrosh, WQ2-Classic leonczuk@outlook.de
## SavedVariables: WQ2_SETTINGS, WQ2_FAVORITES


core\WoWQuote2.lua
core\WoWQuote2.xml
locales\Localization.de.lua
locales\Localization.en.lua

locales\LocalizationUI.de.lua
locales\LocalizationUI.en.lua
gui\WoWQuote2UI.lua
gui\WoWQuote2UI.xml




