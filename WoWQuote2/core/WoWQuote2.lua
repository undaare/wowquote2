-- Konstanten
local WQ2_PREFIX = "WQ2";
WQ2_ADDON_NAME = "WoWQuote2";
WQ2_VERSION = "0.2.3.2.80000";
WQ2_MAX_ALIAS_LEN = 20;
WQ2_MIN_SEARCH_LEN = 3;
local WQ2_MIN_SOUND_DELAY = 3;


--globale Variabeln
WQ2_Tags ={};


-- lokale Variabeln
local moduleData = {};
local newestVersion = WQ2_VERSION;
local soundTimer = time();
local lastSender = nil;
local numPartyMembers = 0;
local soundHandle = nil;
local counter = 0;

--local verRequest = false;

--gespeicherte Einstellungen
WQ2_SETTINGS = { };
WQ2_FAVORITES = { };

local defaults = {
  ["broadcast"] = {
    ["GUILD"] = true,
    ["RAID"] = true,
    ["BATTLEGROUND"] = true,
    ["PARTY"] = true,
  },

  ["quickbutton"] ={
    ["SHOWN"] = true,

  },
  ["aliases"] =  {},

  ["channel"] ={
    ["CHANNEL"] = 2
  },
  ["soundoutput"] = {
     ["CHANNEL"] = "Dialog",
  },
}



function WQ2_init()

  --Checks if new SavedVariables are added
  for key, value in pairs(defaults) do
    if WQ2_SETTINGS[key] == nil then
      WQ2_SETTINGS[key] = value
    end
  end

  C_ChatInfo.RegisterAddonMessagePrefix(WQ2_PREFIX);

  SlashCmdList["WQ"] = WQ2_showUI;
  SLASH_WQ1 = "/wq";

  SlashCmdList["WQNOT"] = WQ2_NotifyRequest;
  SLASH_WQNOT1 = "/wqnot";
  SLASH_WQNOT2 = "/wqn";

  SlashCmdList["WQHELP"] = WQ2_help;
  SLASH_WQHELP1 = "/wqh";
  SLASH_WQHELP2 = "/wqhelp";

  SlashCmdList["WQBROADCAST"] = WQ2_broadcast;
  SLASH_WQBROADCAST1 = "/wqb";
  SLASH_WQBROADCAST2 = "/wqbroadcast";

  SlashCmdList["WQPARTY"] = WQ2_party;
  SLASH_WQPARTY1 = "/wqp";
  SLASH_WQPARTY2 = "/wqparty";

  SlashCmdList["WQRAID"] = WQ2_raid;
  SLASH_WQRAID1 = "/wqr";
  SLASH_WQRAID2 = "/wqraid";

  SlashCmdList["WQGUILD"] = WQ2_guild;
  SLASH_WQGUILD1 = "/wqg";
  SLASH_WQGUILD2 = "/wqguild";

  SlashCmdList["WQLISTEN"] = WQ2_listen;
  SLASH_WQLISTEN1 = "/wql";
  SLASH_WQLISTEN2 = "/wqlisten";

  SlashCmdList["WQALIAS"] = WQ2_alias;
  SLASH_WQALIAS1 = "/wqa";
  SLASH_WQALIAS2 = "/wqalias";

  SlashCmdList["WQFIND"] = WQ2_find;
  SLASH_WQFIND1 = "/wqf";
  SLASH_WQFIND2 = "/wqfind";

  SlashCmdList["WQSTOP"] = WQ2_stop;
  SLASH_WQSTOP1 = "/wqs";
  SLASH_WQSTOP2 = "/wqstop";

  SlashCmdList["WQQBUI"] = WQ2_QBUI;
  SLASH_WQQBUI1 = "/wqqb";



  WQUI:UpdateCheckButtons();
  WQUI.SetID(WQ2_SETTINGS.channel.CHANNEL);
  WQUIB:Switch(WQ2_SETTINGS.quickbutton.SHOWN);

  WQ2_msg("msg_loaded", WQ2_ADDON_NAME, WQ2_VERSION);


  WQ2_importTags(WQ2_BASE_TAGS);
  WQ2:UnregisterEvent("ADDON_LOADED");

end

function WQ2_resetLastSender() --for testing purposes only
  WQ2_print("lastSender \""..tostring(lastSender).."\" will be reset to nil");
  lastSender = nil;
end


--Chatfunctions
--Universal function: trims a string and returns invalid letters
function WQ2_trim(s) return (string.gsub(s, "^%s*(.-)%s*$", "%1")) end
--Universal function: Calls the WQ2_print(msg) function
function WQ2_msg(msg,p1,p2,p3) WQ2_print(WQ2_ADDON_NAME .. ": " .. string.format(WQ2_MSG[msg],p1,p2,p3)); end
--Universal function: Prints a msg, that's it!
function WQ2_print(msg) DEFAULT_CHAT_FRAME:AddMessage(msg,0.5,0.5,0.9); end


--Commandlist
function WQ2_help() WQ2_printTable(WQ2_HELP); end
function WQ2_showUI() WQUI:Toggle(); end
function WQ2_stop() if (soundHandle == nil)then else StopSound(soundHandle); end end

function WQ2_QBUI() WQUIB:Switch(not(WQ2_SETTINGS.quickbutton.SHOWN)); WQ2_SETTINGS.quickbutton.SHOWN = not(WQ2_SETTINGS.quickbutton.SHOWN);end
function WQ2_guild(id) WQ2_send(id, "GUILD"); end
function WQ2_party(id) if GetNumGroupMembers() ~= 0 then WQ2_send(id, "PARTY"); else WQ2_msg("err_not_in_party"); end end

function WQ2_raid(id)
  local inInstance, instanceType = IsInInstance();
  if (inInstance and instanceType == "pvp") then
    WQ2_send(id, "BATTLEGROUND");
  elseif IsInRaid() ~= 0 then
    WQ2_send(id, "RAID");
  else
    WQ2_party(id);
  end
end

function WQ2_listen(id)
  if (not id or WQ2_trim(id) == "") then
    return;
  end

  local moduleid, quoteid, media, path = WQ2_getQuoteByID(id);
  if not media then
    moduleid, quoteid, media, path = WQ2_getQuoteByAlias(id);
  end

  if media then
    WQ2_print(media.id..": [ " .. media.msg .. " ]");
    WQ2_play(media, path);
  else
    WQ2_notFound(id);
  end

end

function WQ2_ChannelConversion(channelNumber)
  if(channelNumber == 1) then
    return "PARTY"
  end
  if(channelNumber == 2) then
    return "GUILD"
  end
  if(channelNumber == 3) then
    return "RAID"
  end
end

function WQ2_Notify(channel, sender)
  --local out = WQ2_channels[channel].color .. "[WOWQuote2] " ..sender.. ": requests Notification!";
  --DEFAULT_CHAT_FRAME:AddMessage(out);
  C_ChatInfo.SendAddonMessage(WQ2_PREFIX, ("cmd=ack"), channel);
end

function WQ2_NotifyRequest()
  local channel = WQ2_ChannelConversion(WQ2_SETTINGS.channel.CHANNEL);
  local out = WQ2_channels[channel].color .. "[WOWQuote2] " .. "You requested Notification!";
  DEFAULT_CHAT_FRAME:AddMessage(out);
  C_ChatInfo.SendAddonMessage(WQ2_PREFIX, ("cmd=not"), channel);
end

function WQ2_ack(channel, sender)
  local out = WQ2_channels[channel].color .. "[WOWQuote2] " ..sender.. " uses WoWQuote2!";
  DEFAULT_CHAT_FRAME:AddMessage(out);
end

function WQ2_broadcast(cmd)

  -- Einstellungen anzeigen
  if tostring(cmd) == "" then
    WQ2_showSettings();
    return;
  end

  local arg1, arg2 = WQ2_getCmd(cmd);

  arg1 = string.lower(arg1);
  arg2 = string.lower(arg2);

  --2. argument prüfen, da erstes im spezialfall all nicht geprüft werden darf
  if (arg2 ~= "on" and arg2 ~= "off") then
    WQ2_msg("err_miss_switch");
    return;
  end

  -- spezialfall: alle channel an und aus schalten
  if (arg1 == "all") then
    WQ2_broadcast("p " .. arg2);
    WQ2_broadcast("g " .. arg2);
    WQ2_broadcast("r " .. arg2);
    return;
  end

  -- 1. argument prüfen für normalen Fall
  if not string.find(arg1,"[prg]") then
    WQ2_msg("err_miss_channel");
    return;
  end


  -- einzelne Channels an und abstellen
  if arg2 == "on" then
    if arg1 == "p" then
      WQ2_SETTINGS.broadcast.PARTY = true;
      WQ2_msg("msg_chan_on", WQ2_channels.PARTY.name);
    end
    if arg1 == "g" then
      WQ2_SETTINGS.broadcast.GUILD = true;
      WQ2_msg("msg_chan_on", WQ2_channels.GUILD.name);
    end
    if arg1 == "r" then
      WQ2_SETTINGS.broadcast.RAID = true;
      WQ2_SETTINGS.broadcast.BATTLEGROUND = true;
      WQ2_msg("msg_chan_on", WQ2_channels.RAID.name);
    end
  end

  if arg2 == "off" then
    if arg1 == "p" then
      WQ2_SETTINGS.broadcast.PARTY = false;
      WQ2_msg("msg_chan_off", WQ2_channels.PARTY.name);
    end
    if arg1 == "g" then
      WQ2_SETTINGS.broadcast.GUILD = false;
      WQ2_msg("msg_chan_off", WQ2_channels.GUILD.name);
    end
    if arg1 == "r" then
      WQ2_SETTINGS.broadcast.RAID = false;
      WQ2_SETTINGS.broadcast.BATTLEGROUND = false;
      WQ2_msg("msg_chan_off", WQ2_channels.RAID.name);
    end
  end

  -- UI Updaten
  WQUI:UpdateCheckButtons();

end

function WQ2_send(id, system)
  if (not WQ2_SETTINGS.broadcast[system]) then
    WQ2_msg("err_sendchan_off", WQ2_channels[system].name);
    return;
  end
  if (not id or WQ2_trim(id) == "") then
    return;
  end

  local moduleid, quoteid, media = WQ2_getQuoteByID(id);
  if not media then
    moduleid, quoteid, media = WQ2_getQuoteByAlias(id);
  end

  if media then
    C_ChatInfo.SendAddonMessage(WQ2_PREFIX, ("cmd=play, moduleid="..moduleid..", quoteid="..moduleid..":"..quoteid), system); -- senden neues Protokoll das benutzt man ums unter leute zu schicken
    if moduleid == "default" then
      C_ChatInfo.SendAddonMessage(WQ2_PREFIX, quoteid, system);
    elseif moduleid == "wq2" then
      C_ChatInfo.SendAddonMessage(WQ2_PREFIX, quoteid+799, system);
    end
  else
    WQ2_notFound(id);
  end
end

function WQ2_showSettings()
  WQ2_print(string.format(WQ2_MSG["msg_conf_title"],WQ2_ADDON_NAME));
  local tab = {};
  for i,v in pairs(WQ2_SETTINGS.broadcast) do
    if v then
      tab[WQ2_channels[i]["name"]] = "on";
    else
      tab[WQ2_channels[i]["name"]] = "off";
    end
  end
  WQ2_printTable(tab, true);
end

function WQ2_getQuoteByID(id)
  --WQ2_print("WQ2_getQuoteByID("..id..")");
  --WQ2_print(tostring(string.match(id, "%s*(%w*):?(%w*)%s*")));

  local moduleid=nil;
  local quoteid=nil;
  local media=nil;
  local path=nil;

  if id and WQ2_trim(id)~="" then
    moduleid, quoteid = string.match(id, "%s*(%w*):?(%w*)%s*");
    if quoteid == "" then
      quoteid=moduleid;
      moduleid="default";
    end
    --WQ2_print("WQ2_getQuoteByID(): resolved moduleid: "..tostring(moduleid).." quoteid: "..tostring(quoteid));
    media, path = WQ2_getMedia(moduleid, quoteid);
    --WQ2_print("WQ2_getQuoteByID(): resolved media: "..tostring(media));
  end

  return moduleid, quoteid, media, path;
end

function WQ2_getQuoteByAlias(alias)
  if WQ2_SETTINGS.aliases[alias] then
    return WQ2_getQuoteByID(WQ2_SETTINGS.aliases[alias]);
  else
    return nil, nil, nil, nil;
  end

end

--Called by Send Message I think

function WQ2_onEvent(event, ...)

  --switch events
  if (event == "ADDON_LOADED" and select(1,...) == WQ2_ADDON_NAME) then  --why VARIABLES_LOADED? check!
    WQ2_init();
  elseif (event == "PLAYER_ENTERING_WORLD") then
    WQ2_sendVersion("GUILD", true);
    WQ2:UnregisterEvent("PLAYER_ENTERING_WORLD");
  elseif (event == "CHAT_MSG_ADDON" and select(1,...) == WQ2_PREFIX) then
    -- WQ2 AddonMessage
    local args = {...};
    local cmdlist;

    --switch protocols
    if (string.find(args[2], "=")) then
      --new protocol
      cmdlist =  WQ2_parseArguments(args[2]);
      lastSender = args[4];
    elseif (lastSender ~= args[4] and tonumber(args[2])) then
      --old protocol
      --convert old protocol event to new protocol event --> handle as play cmd
      cmdlist = WQ2_convertOldProtocol(args[2]);
    else
      return;
    end

    --switch comands
    if (cmdlist.cmd == "play") then
      --handle play cmd
      WQ2_handlePlayCmd(cmdlist, args[3], args[4])
    elseif (cmdlist.cmd == "ver") then
      --handle ver cmd
      WQ2_handleVersion(cmdlist, args[3]);
    elseif (cmdlist.cmd == "ack") then
      WQ2_ack(args[3],args[4]);
    elseif (cmdlist.cmd == "not") then
      WQ2_Notify(args[3],args[4]);
      --WQ2_print("unknown cmd, cmdlist:")
      --WQ2_printTable(cmdlist,true);

    end

  elseif (event == "GROUP_ROSTER_UPDATE") then
    -- send Version into Party/Raid
    local args = {...};
    WQ2_partyMembersChanged();
  end

end


function WQ2_handlePlayCmd(cmdlist, system, sender)
  if (cmdlist.moduleid and cmdlist.quoteid) then
    local media, path = WQ2_getMedia(cmdlist.moduleid, cmdlist.quoteid);
    if (media and path and WQ2_SETTINGS.broadcast[system]) then
      if (cmdlist.silent ~= "true") then
        WQ2_showMsg(sender, media.msg, media.len, system);
      end
      WQ2_play(media, path);
    else
      --WQ2_print("handlePlayCmd: No playable media found");
    end
  end
end

function WQ2_convertOldProtocol(id)
  local cmdlist = {["cmd"] = "play", };
  id = tonumber(id);

  if (id > 800) then
    cmdlist.moduleid = "wq2";
    cmdlist.quoteid = id - 799;
  else
    cmdlist.moduleid = "default";
    cmdlist.quoteid = id;
  end

  --WQ2_print("old protocol converted:");
  --WQ2_printTable(cmdlist, true);

  return cmdlist;
end

--Message when a players sends a sound

function WQ2_showMsg(sender, msg, t, system)
  local playerlink = "|Hplayer:" .. sender .. "|h[" .. sender .. "]|h";
  local out = WQ2_channels[system].color .. "[" .. WQ2_channels[system].name .. "] " .. playerlink .. ": [ " .. msg .. " | Duration: "..t.." seconds".." ]";
  DEFAULT_CHAT_FRAME:AddMessage(out);
end


function WQ2_partyMembersChanged()

  local num = GetNumGroupMembers();
  local inInstance, instanceType = IsInInstance();

  if (num > numPartyMembers and newestVersion == WQ2_VERSION) then
    --WQ2_print("WQ2_sendVersion(\"RAID\");");
    if (inInstance and instanceType == "pvp") then
      WQ2_sendVersion("BATTLEGROUND");
    else
      WQ2_sendVersion("RAID");
    end
  elseif (num == 0) then
    num = GetNumSubgroupMembers();
    if (num > numPartyMembers and newestVersion == WQ2_VERSION) then
      --WQ2_print("WQ2_sendVersion(\"PARTY\");");
      WQ2_sendVersion("PARTY");
    end
  end
  numPartyMembers = num;

  --WQ2_print("PARTY_MEMBERS_CHANGED: "..numPartyMembers);
end

function WQ2_handleVersion(cmdlist, system)
  --WQ2_print("handleVersion: "..tostring(cmdlist.ver));
  if (not cmdlist.ver) then
    --keine Version mit übertragen --> request --> senden
    WQ2_sendVersion(system, false, (cmdlist.force == "true"));
  elseif (cmdlist.mod == WQ2_ADDON_NAME and cmdlist.ver>newestVersion) then
    --WQ2 betreffende Versionsinfo mit neuerer Version
    newestVersion = cmdlist.ver;
    getglobal(WQUI:GetName().."Title"):SetTextColor(1,0,0);
    getglobal(WQUI:GetName().."Title"):SetText(string.format(WoWQuote2UI_Localization.DIALOG_TITLE_NEW_VERSION,newestVersion));
  end

end

function WQ2_sendVersion(system, request, force)
  --WQ2_print("WQ2_sendVersion("..system..")");
  if (request) then
    C_ChatInfo.SendAddonMessage(WQ2_PREFIX, "cmd=ver", system);
  elseif (newestVersion == WQ2_VERSION or force) then
    C_ChatInfo.SendAddonMessage(WQ2_PREFIX, ("cmd=ver, ver="..WQ2_VERSION..", mod="..WQ2_ADDON_NAME), system);
  end
end


function WQ2_importTags(tags)
  for key, value in pairs(tags) do
    if (not WQ2_Tags[key]) then
      WQ2_Tags[key] = value
    end
  end
end

function WQ2_getModuleData() return moduleData; end

function WQ2_InitModule(module)
  moduleData[module.moduleid] = module;
  if module.name then
    if (not WQ2_Tags[module.moduleid]) then
      WQ2_Tags[module.moduleid] = module.name
    end
  end
  if module.tags then
    WQ2_importTags(module.tags);
  end
  --WQ2_print("module inited: "..module.moduleid);
end

function WQ2_getMedia(moduleid, quoteid)
  --WQ2_print("WQ2_getMedia("..moduleid..", "..quoteid.."); type(quoteid): "..type(quoteid));

  local media = nil;
  local path = nil;
  local index;
  quoteid = tostring(quoteid);
  if string.find(quoteid, ":") then
    quoteid = string.match(quoteid, "%s*[^%:]*%:(%d*)")
    index = tonumber(quoteid);
  else
    index = tonumber(quoteid);
  end
  --WQ2_print("getMedia: index: "..tostring(index));
  --WQ2_print("getMedia: quoteid: "..quoteid);
  --WQ2_print("moduleData[moduleid].mediadata[index].id == quoteid "..tostring(moduleData[moduleid].mediadata[index].id == quoteid));

  if (moduleData[moduleid]) then --modul vorhanden

    if (index and index<=#moduleData[moduleid].mediadata and moduleData[moduleid].mediadata[index] and (moduleData[moduleid].mediadata[index].id == quoteid or moduleData[moduleid].mediadata[index].id == moduleid..":"..quoteid)) then
      media = moduleData[moduleid].mediadata[index];
      --WQ2_print("getMedia: found by index");
    else
      --WQ2_print("getMedia: searching...");
      for i,v in pairs(moduleData[moduleid].mediadata) do
        --WQ2_print(v.id);
        if (v.id == quoteid or v.id == moduleid..":"..quoteid) then
          media = v;
          --WQ2_print("getMedia: found by search");
          break;
        end
      end
    end
    path = moduleData[moduleid].mediapath;
  else
    --WQ2_print("moduleid not found: "..tostring(moduleData[moduleid]));
  end

  --WQ2_print("getMedia() returns: "..tostring(media)..", "..tostring(path));
  return media, path;

end


function WQ2_parseArguments(input)
  -- returns a keyed table of values
  -- arguments are divided by commata (",")
  -- keys are divided from their values by an equal sign ("=")
  -- keys and values may not begin or end with whitespace characters
  --    (whitespace is trimmed)
  -- keys and values may not contain comma or equal sign characters ("," "=")
  --    (there is currently no way to escape special characters; this may change in the future)
  local result = {}
  local length = string.len(input)
  local argument
  local cursor = 1
  while cursor <= length do
    argument, cursor = string.match(input, "([^,]*),?()", cursor)
    local key, value = string.match(argument, "%s*([^=%s]*)%s*=%s*(.*)%s*$")
    if key and value then
      result[key] = value
    else
      -- error: argument without "="
    end
  end
  return result
end

--Universal Function: Plays the media.
--Changes made => if the Duration (delay) is greater than 10 seconds then you can't play
--another file that is longer than 10 seconds, but you can still play shorter sounds.
--You can spam shorter sounds or play them while the music is playing.
--Old music stops playing when someone else plays a new music.
--Sound Output: changed to "Dialog" Sound channel.

function WQ2_play(media, path)                     --
  local delay = WQ2_MIN_SOUND_DELAY;
  local willPlay;
  local merker
  local file = path .. media.file;
  if (type(media.len)=="number") then
    delay = media.len;
  end

  if (delay >= 10) then
    delay = 10;
  else
    PlaySoundFile(file, WQ2_SETTINGS.soundoutput.CHANNEL);
  end
  if (time() >= soundTimer) then
    soundTimer = time()+delay;
    if (delay == 10) then
      if(soundHandle == nil) then
      else
        StopSound(soundHandle)
      end
      willPlay, soundHandle = PlaySoundFile(file, WQ2_SETTINGS.soundoutput.CHANNEL);
    end
  end
  --C_Timer.After(3, WQ2_singleStop(soundHandle));
  --if (merker > 0) then
  --C_Timer.After(media.len+1, function() then StopSound(soundHandle[merker]) end);
  --end
end

--Stops the current playing sound file if it's duration is longer than 10 seconds
--Function is called by the stopButton in WoWQuote2UI.xml and the play function



--Chat Command function: /wqf chat command

function WQ2_find(str)
  if str ~= nil then
    str = WQ2_trim(string.lower(str));
  end
  if string.len(str) < WQ2_MIN_SEARCH_LEN then
    WQ2_msg("err_search_len",WQ2_MIN_SEARCH_LEN);
    return;
  end;
  local found, count = WQ2_search(str);
  WQ2_msg("msg_search_count",count);

  local foundToPrint = {};

  for i,v in pairs(found) do
    foundToPrint[v.id] = v.msg;
  end
  WQ2_printTable(foundToPrint, true);


end;

--Chat Command function: Searches all modules for any media containinfg the str String

function WQ2_search(str)
  local result = {};
  local searchCount = 0;
  str = string.lower(str);

  for moduleid, module in pairs(moduleData) do
    --WQ2_print("WQ2_search: searching in module: "..tostring(module.name));
    for id, media in pairs(module.mediadata) do
      --WQ2_print("WQ2_search: searching in quote: "..tostring(media.id).." --> "..media.msg) ;
      if string.find(string.lower(media.msg), str) then
        --WQ2_print("WQ2_search: found: "..tostring(media.msg));
        table.insert(result, media);
        searchCount= searchCount+1;
      end
    end
  end

  --WQ2_print("WQ2_search: "..searchCount.." matches found for \""..str.."\"");
  return result, searchCount;
end

--Chat Command function: returns a valid command

function WQ2_getCmd(msg)
  if (msg) then
    local a,b=string.find(msg, "[^%s]+");
    if (not ((a==nil) and (b==nil))) then
      local cmd=string.lower(string.sub(msg,a,b));
      return cmd, string.sub(msg,string.find(cmd,"$")+1);
    else
      return "";
    end;
  end;
end;

--Universal function: Error if the quote wasn't found

function WQ2_notFound(id)
  WQ2_msg("err_quote_not_found",id);
  PlaySound("TellMessage");
end

--Chat Command function: Prints a list I guess

function WQ2_printTable(t,show)
  for i,v in pairs(t) do
    local msg = "|cff7090ff" .. tostring(v);
    if (show) then
      msg = "|cffffffff" .. tostring(i) .. " : " .. msg;
    end
    DEFAULT_CHAT_FRAME:AddMessage(msg,0.5,0.5,0.9);
  end
end

--Chat Command function: I don't like those alias things tho they are kinda cool

function WQ2_alias(cmd)                              --setzt Aliasse mit /wqa
  local alias, id = WQ2_getCmd(cmd);
  if (alias == nil or WQ2_trim(alias) == "") then     --gesetzte Aliasse anzeigen für keine Parameter
    WQ2_msg("msg_show_aliases");
    WQ2_printTable(WQ2_SETTINGS.aliases, true);
    return;
  end

  if (id == nil or WQ2_trim(id) == "") then           --alias Löschen für nur einen Parameter
    if (WQ2_SETTINGS.aliases[alias]) then
      WQ2_SETTINGS.aliases[alias] = nil;
      WQ2_msg("msg_alias_disabled",alias);
    else
      WQ2_msg("err_alias_not_found",alias);
    end;
    return;
  end
  -- alias auf id setzen, falls id gültig
  local moduleid, quoteid, media = WQ2_getQuoteByID(id);

  if not media then                                   --id gültig?
    WQ2_notFound(id);
    return;
  end

  if alias ~= nil and alias ~= "" and string.len(alias)<WQ2_MAX_ALIAS_LEN and string.find(alias,'^[%a]+[%w]*$') ~= nil then --alias Gültig?
    WQ2_SETTINGS.aliases[alias] = media.id;
    WQ2_msg("msg_alias_set",alias, media.id);
    return;
  else
    WQ2_msg("err_wrong_alias");
  end
end

--Universal Function: returns filtered Media and returns the list

function WQ2_getFilteredMedia(tag)
  --WQ2_print("getFilteredMedia("..tostring(tag)..")");
  --WQ2_print("getFilteredMedia: moduleData length: "..#moduleData);
  local result = {};

  if tag then
    for moduleid, module in pairs(moduleData) do
      for i, media in pairs(module.mediadata) do
        if media.tags then
          for i, quotetag in ipairs(media.tags) do
            if quotetag == tag[1] then
              table.insert(result, media);
            end
          end
        end
        if tag[1] == moduleid then
          table.insert(result, media);
        end
      end
    end
  else
    for moduleid, module in pairs(moduleData) do
      for i, media in pairs(module.mediadata) do
        table.insert(result, media);
      end
    end
  end

  --WQ2_print("getFilteredMedia: return table length: "..#result);

  return result;
end

function WQ2_favorite(id)
	if WQ2_isFavorite(id) then
		WQ2_unsetFavorite(id)
	else
		WQ2_setFavorite(id)
	end
end

function WQ2_isFavorite(id)
	for i, favorite in pairs(WQ2_FAVORITES) do
		if favorite == id then
			return true
		end
	end
	return false
end

function WQ2_setFavorite(id)
	table.insert(WQ2_FAVORITES, id)
	WQ2_msg("msg_fav_set", id)
end

function WQ2_unsetFavorite(id)
	for i, favorite in pairs(WQ2_FAVORITES) do
		if favorite == id then
			table.remove(WQ2_FAVORITES, i)
			WQ2_msg("msg_fav_unset", id)
		end
	end
end
